/* 100명이 있을 때 한 번의 승리자가 결정 될 수 있는 조합은 몇가지 인가?
*
*
* */
let N = 4;

let cnt = 0;

for(let rock = 0; rock <= N; rock++) {
	for(let paper = 0; paper <= N - rock; paper++) {
		let scissor = N - rock - paper;
		if(rock > scissor) {
			if(rock != paper)
				cnt++;
		}else if(rock < scissor) {
			if(scissor != paper)
				cnt++;
		}else {
			if(rock < paper)
				cnt++;
		}
	}
}

console.log(cnt);


