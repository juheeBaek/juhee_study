/*
* 7개의 로마 문자 I, V, L, C, D, M
* 4자리 연속으로 올 수 없다.
*
* 12개를 나열한다.
* V, L, D는 한개씩밖에 올 수 없다.
*
* */
N =12
// 자릿수 하나 반환
function conv(n,i, v, x) {
	var result = '';
	if(n == 9) {
		result += i +x;
	}else if(n == 4) {
		result += i + v;
	}
	else {
		for(var j = 0; j < Math.floor(n / 5); j++) {
			result += v;
		}
		n = n % 5;
		for(var j = 0; j < n; j++) {
			result += i;
		}
	}
	return result;
}

// 로마 숫자로 변환
function roman(n) {
	var m = Math.floor(n / 1000);
	n %= 1000;
	var c =  Math.floor(n / 100);
	n %= 100;
	var x = Math.floor(n / 10);
	n %= 10;

	var result = 'M'.repeat(m);
	result += conv(c, 'C', 'D', 'M');
	result += conv(x, 'X', 'L', 'C');
	result += conv(n, 'I', 'V', 'X');

	return result;
}

var cnt = {};
for (i = 0; i < 4000; i++) {
	var len = roman(i).length;
	if(cnt[len]) {
		cnt[len] += 1;
	}else {
		cnt[len] = 1;
	}
}

console.log(cnt[N]);