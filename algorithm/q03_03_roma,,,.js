/*
* 7개의 로마 문자 I, V, L, C, D, M
* 4자리 연속으로 올 수 없다.
*
* 12개를 나열한다.
* V, L, D는 한개씩밖에 올 수 없다.
*
* */

var arr = [1, 5, 10, 50, 100, 500, 1000];

var rome = ['I', 'V', 'X', 'L', 'C', 'D', 'M'];

var obj = [
	{ rome: 'I', number: 1, limit: 3},
	{ rome: 'V', number: 5, limit: 1},
	{ rome: 'X', number: 10, limit: 3},
	{ rome: 'L', number: 50, limit: 1},
	{ rome: 'C', number: 100, limit: 3},
	{ rome: 'D', number: 500, limit: 1},
	{ rome: 'M', number: 1000, limit: 3}
	];

var res = [];

for(var i = 1; i <= 3999; i++) {
	var sum = 0;
	obj.forEach(function (item, idx) {
		sum += obj[idx].number;
		if(sum == i) {
			console.log(sum);
		}
	});
}






