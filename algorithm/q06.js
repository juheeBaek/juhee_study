
W = 1000;
N = 20;
var cnt = 0;

for(var i = 1; i <= W; i++) {
	for(var j = i;  j <= W; j++) {
		res = cut(i, j);
		if(res === N) {
			cnt++;
		}
	}
}

console.log(cnt);

function cut(w, h) {
	if(w == h) return 1;
	if(w > h) {
		var tmp = w;
		w = h;
		h = tmp;
	}
	var r = h % w;
	var result = Math.floor(h / w);
	if (r > 0) result += cut(w, r);
	return result;
}