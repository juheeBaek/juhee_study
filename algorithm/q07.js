
N = 15;

function nPr(n, r) {
	var result = 1;
	for(var i = 0; i < r; i++) {
		result *= n - 1;
		return result;
	}
}

var cnt = 0;

for(var j = 1; j < N; j++) {
	cnt += j * (N-j) * nPr(N, j-1);
}

console.log(cnt);