$(function() {

  var num = 8; // the total number of images 
  
  // Preload all the images into hidden div
  for (var i=1 ; i<=num ; i++) {
      var img = document.createElement('img');
      img.src = 'images/0'+i+'.png';
      document.getElementById('preload-imgs').appendChild(img);
  }
  
  // Control swipes using jquery.touchSwipe.min.js
  // http://labs.rampinteractive.co.uk/touchSwipe/
  var swipeOptions=
  {
      triggerOnTouchEnd : true,	
      swipeStatus : swipeStatus,
      allowPageScroll:"vertical",
      threshold:75			
  }
  
  $(function()
  {				
      imgs = $(".img-container"); // the element that will be swipeable
      console.log(imgs);
      imgs.swipe( swipeOptions );
  });
  
  function swipeStatus(event, phase, direction, distance) {
      var duration = 0;
      if(direction == "left") {
          changeImg(distance);
      }
      else if (direction == "right") {
          changeImgR(-distance);
      }
  }
  
  function changeImg (imgNum) {
      console.log(imgNum);
  
      // divide by 8 (or any number) to spread 
      // it out so it doesn't load new img 
      // every single px of swipe distance
      imgNum = Math.floor(imgNum/5); 
  
      if (imgNum < 1) {
          imgNum += num;
      }
      if (imgNum > num) {
          imgNum -= num;
      }
  
      // change the image src
      document.getElementById("myImg").src="images/0"+imgNum+".png";
  }
  
  function changeImgR (imgNum) {
        console.log(imgNum);
      // divide by 8 (or any number) to spread 
      // it out so it doesn't load new img 
      // every single px of swipe distance
      imgNum = Math.floor(imgNum/5); 
  
      var num2 = -Math.abs(num); 
      if (imgNum > num2) {
          imgNum += num;
      }
      if (imgNum <= num2) {
          imgNum += num*2;
      }
  
      // change the image src
      document.getElementById("myImg").src="images/0"+imgNum+".png";
  }
  })