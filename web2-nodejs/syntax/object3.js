var q = {
    v1: 'v1',
    v2: 'v2',
    f1: function() {
        console.log(this.v1);
    },
    f2: function() {
        console.log(this.v2);
    }
}


q.f1();
q.f2();

// 객체는 값을 담는 그릇이다.
// 함수는 값이다.